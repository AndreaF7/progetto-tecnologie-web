<?php

class DataBaseHelper{
    private $db;

    public function __construct($servername, $username, $password, $dbname){
        $this->db = new mysqli($servername, $username, $password, $dbname);
        if($this->db->connect_error){
            die("Connection failed: " . $this->db->connect_error);
        }
    }

    /*

    USER RELATED FUNCTIONS

    */

    public function getUserByID($ID){
        $query = "SELECT *
                FROM utenti
                WHERE username = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("i", $ID);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function checkLogin($email, $password){
        $query = "SELECT username, tipo, nome FROM utenti WHERE email = ? and password = ?";
        $stmt = $this->db->prepare($query);
        $password = crypt($password, '_J9..rasm');
        $stmt->bind_param('ss',$email,$password);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function checkUser($username, $email){
        $query = "SELECT username,email FROM utenti WHERE email = ? or username = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ss',$email,$username);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function registerUser($username,$nome,$cognome,$data,$email,$password,$iva,$tipo){
        $dt = date('Y-m-d',strtotime($data));
        $query = "INSERT INTO utenti(`username`, `nome`, `cognome`, `data_nascita`, `email`, `password`, `partita_IVA`, `tipo`) 
        VALUES (?,?,?,?,?,?,?,?)";
        $stmt = $this->db->prepare($query);
        $password = crypt($password, '_J9..rasm');
        $stmt->bind_param('ssssssss',$username,$nome,$cognome,$dt,$email,$password,$iva,$tipo);
        $stmt->execute();
    }

    public function modifyUser($username,/*$info,$newdata*/ $name, $surname, $birthDate, $email){
        $query = "UPDATE utenti SET nome=?, cognome=?, data_nascita=?, email=? WHERE username = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('sssss',$name, $surname, $birthDate, $email, $username);
        $stmt->execute();
    }

    public function modifyPassword($username,$oldpassword,$newpassword){
        $oldpassword = crypt($oldpassword, '_J9..rasm');
        $query = "SELECT * FROM utenti WHERE username = ? AND `password` = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ss',$username,$oldpassword);
        $stmt->execute();
        $result = $stmt->get_result();
        if(count($result->fetch_all(MYSQLI_ASSOC))==0){
            return 0;   //Case the old password isn't correct for that user
        }else{
            $newpassword = crypt($newpassword, '_J9..rasm');
            $query = "UPDATE utenti SET `password` = ? WHERE username = ?";
            $stmt = $this->db->prepare($query);
            $stmt->bind_param('ss',$newpassword,$username);
            $stmt->execute();
        }
    }

    /*

    EVENTS RELATED FUNCTIONS

    */

    public function getEventByID($ID){
        $query = "SELECT E.*, L.*, SUM(A.quantità) AS comprati
                FROM eventi AS E JOIN luoghi AS L ON E.ID_Luogo = L.ID_Luogo JOIN acquisti_utenti AS A ON E.ID_evento = A.ID_evento 
                WHERE E.ID_evento = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("i", $ID);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getEventsByTextSearch($text, $limit){
        $query = "SELECT E.*, L.*
                FROM eventi AS E JOIN luoghi AS L ON E.ID_Luogo = L.ID_Luogo 
                WHERE";
        $token = strtok($text, " ");
        while ($token !== false) {
            $token = "'%" . $token . "%'";
            $query = $query . " E.Titolo LIKE " . $token . " OR E.Tipologia LIKE " . $token . " OR L.città LIKE " . $token . " OR E.Username_creatore LIKE " . $token;
            $token = strtok(" ");
            if($token !== false){
                $query = $query . " OR";
            }
        }
        $query = $query . " LIMIT ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("i", $limit);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getEventsByUser($user, $limit){
        $query = "SELECT E.*, L.*
                FROM eventi AS E JOIN luoghi AS L ON E.ID_Luogo = L.ID_Luogo";
        if(!($user == "admin")){
            $query.=" WHERE E.Username_creatore = ? LIMIT ?";
            $stmt = $this->db->prepare($query);
            $stmt->bind_param("si",$user,$limit);
            $stmt->execute();
            $result = $stmt->get_result();

            return $result->fetch_all(MYSQLI_ASSOC);
        }else{
            $query.= " LIMIT ?";
            $stmt = $this->db->prepare($query);
            $stmt->bind_param("i",$limit);
            $stmt->execute();
            $result = $stmt->get_result();

            return $result->fetch_all(MYSQLI_ASSOC);
        }
    }

    public function getEventsOnHome($loc,$giorno,$tipo,$limit){
        $query = "SELECT E.*, L.* FROM eventi AS E JOIN luoghi AS L ON E.ID_Luogo = L.ID_Luogo 
                AND L.città = ? AND E.Tipologia = ? AND E.data = ?
                LIMIT ?";
        $dt = date('Y-m-d',strtotime($giorno));
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("sssi",$loc,$tipo,$dt,$limit);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getNewEvents($limit){
        $query = "SELECT * FROM eventi ORDER BY `data_inserimento` LIMIT ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("i",$limit);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getNearEvents($limit){
        $query = "SELECT * FROM eventi WHERE `data` >= CURDATE() ORDER BY `data` LIMIT ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("i",$limit);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getMostParticipatedEvents($limit){
        $query = "SELECT E.*
                FROM eventi AS E JOIN (SELECT DISTINCT A1.ID_evento
                                    FROM acquisti_utenti AS A1
                                    ORDER BY (SELECT SUM(A2.quantità)
                                            FROM acquisti_utenti AS A2
                                            WHERE A1.ID_evento = A2.ID_evento) DESC
                                    LIMIT ?) AS A3
                where E.ID_evento = A3.ID_evento";
        $stmt = $this->db->prepare($query);
        if ($stmt == false){
           return var_dump($stmt);
        } else {
            $stmt->bind_param("i",$limit);
            $stmt->execute();
            $result = $stmt->get_result();

            return $result->fetch_all(MYSQLI_ASSOC);
        }

    }

    public function getZones(){
        $query = "SELECT DISTINCT città FROM luoghi";
        $stmt = $this->db->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getTypes(){
        $query = "SELECT DISTINCT Tipologia FROM eventi";
        $stmt = $this->db->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function addBoughtEventTickets($IDEvent, $username, $nTickets){
        $query = "INSERT INTO acquisti_utenti (ID_evento, username, quantità)
                VALUES (?, ?, ?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("isi", $IDEvent, $username, $nTickets);
        $stmt->execute();
        $result = $stmt->get_result();
    }

    public function addEvent($title, $maxTicket, $price, $PlaceID, $eventDate, $Type, $creatorUsername, $text, $image, $creationDate){
        $query = "INSERT INTO acquisti_utenti (Titolo, max_biglietti, prezzo, ID_luogo, data_evento, Tipologia, Username_creatore, Testo, Immagine, data_inserimento)
                VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("siiissssss", $title, $maxTicket, $price, $PlaceID, $eventDate, $Type, $creatorUsername, $text, $image, $creationDate);
        $stmt->execute();
        $result = $stmt->get_result();
    }


    public function checkEvent($title, $PlaceID, $eventDate){
        $query = "SELECT Titolo,ID_luogo,data_evento FROM eventi WHERE Titolo = ? and ID_luogo = ? and data_evento = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('sis',$title,$PlaceID,$eventDate);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    /* 

        PLACES RELATED FUNCTIONS

    */
    
    public function addPlace($city, $address){
        $query = "INSERT INTO luoghi (città, indirizzo)
                VALUES (?, ?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("ss", $city, $address);
        $stmt->execute();
    }


    public function getPlace($city, $address){
        $query = "SELECT ID_luogo FROM luoghi WHERE città = ? AND indirizzo = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("ss", $city, $address);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }
     
}

?>