-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Dic 17, 2019 alle 17:05
-- Versione del server: 10.4.8-MariaDB
-- Versione PHP: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `age_db`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `acquisti_utenti`
--

CREATE TABLE `acquisti_utenti` (
  `quantità` int(11) DEFAULT NULL,
  `ID_biglietto` int(11) NOT NULL,
  `username` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `eventi`
--

CREATE TABLE `eventi` (
  `ID_evento` int(11) NOT NULL,
  `Titolo` varchar(45) NOT NULL,
  `max_biglietti` int(11) NOT NULL,
  `ID_luogo` int(11) NOT NULL,
  `data` date NOT NULL,
  `data_inserimento` date NOT NULL,
  `Tipologia` varchar(30) NOT NULL,
  `Username_creatore` varchar(45) NOT NULL,
  `Testo` mediumtext NOT NULL,
  `Immagine` tinytext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `eventi`
--

INSERT INTO `eventi` (`ID_evento`, `Titolo`, `max_biglietti`, `ID_luogo`, `data`, `data_inserimento`, `Tipologia`, `Username_creatore`, `Testo`, `Immagine`) VALUES
(2, 'Merenda da Alan', 10, 1, '2019-12-29', '2019-12-17', 'Merenda', 'Frenk', 'Siete tutti i benvenuti alla prossima merenda a base di pane senza glutine a casa di Alan Ford', 'merenda.png');

-- --------------------------------------------------------

--
-- Struttura della tabella `luoghi`
--

CREATE TABLE `luoghi` (
  `ID_luogo` int(11) NOT NULL,
  `città` varchar(45) DEFAULT NULL,
  `indirizzo` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `luoghi`
--

INSERT INTO `luoghi` (`ID_luogo`, `città`, `indirizzo`) VALUES
(1, 'Riccione', 'Via Rossi'),
(2, 'Misano', 'Via Rossi');

-- --------------------------------------------------------

--
-- Struttura della tabella `notifiche`
--

CREATE TABLE `notifiche` (
  `id_notifica` int(11) NOT NULL,
  `Titolo` varchar(45) DEFAULT NULL,
  `Testo` varchar(800) DEFAULT NULL,
  `ID_evento` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `notifiche_utenti`
--

CREATE TABLE `notifiche_utenti` (
  `username` varchar(45) NOT NULL,
  `id_notifica` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `tipologie_biglietto`
--

CREATE TABLE `tipologie_biglietto` (
  `ID_biglietto` int(11) NOT NULL,
  `prezzo` decimal(2,0) DEFAULT NULL,
  `zona` varchar(45) DEFAULT NULL,
  `ID_evento` int(11) DEFAULT NULL,
  `quantità` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='				';

-- --------------------------------------------------------

--
-- Struttura della tabella `utenti`
--

CREATE TABLE `utenti` (
  `username` varchar(45) NOT NULL,
  `nome` varchar(45) DEFAULT NULL,
  `cognome` varchar(45) DEFAULT NULL,
  `data_nascita` date DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `partita_IVA` varchar(11) DEFAULT NULL,
  `tipo` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `utenti`
--

INSERT INTO `utenti` (`username`, `nome`, `cognome`, `data_nascita`, `email`, `password`, `partita_IVA`, `tipo`) VALUES
('Ferro', 'Andrea', 'Ferretti', '1998-04-22', 'andrea@age', '5e32a7b30eff59b39b327c471f55a69e', '', 'A'),
('Frenk', 'Franco', 'Bertoldo', '2019-12-02', 'b@b', '92eb5ffee6ae2fec3ad71c777531578f', 'b', 'O'),
('jhonny', 'Jhonny', 'Stecchino', '2019-12-02', 'j@j', '363b122c528f54df4a0446b6bab05515', NULL, 'U');

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `acquisti_utenti`
--
ALTER TABLE `acquisti_utenti`
  ADD PRIMARY KEY (`ID_biglietto`,`username`),
  ADD KEY `Biglietti_foreign_key` (`ID_biglietto`) USING BTREE,
  ADD KEY `Utenti_acquisto_foreign_key` (`username`);

--
-- Indici per le tabelle `eventi`
--
ALTER TABLE `eventi`
  ADD PRIMARY KEY (`ID_evento`),
  ADD KEY `Luoghi_foreign_key` (`ID_luogo`),
  ADD KEY `Creatore_foreign_key` (`Username_creatore`);

--
-- Indici per le tabelle `luoghi`
--
ALTER TABLE `luoghi`
  ADD PRIMARY KEY (`ID_luogo`);

--
-- Indici per le tabelle `notifiche`
--
ALTER TABLE `notifiche`
  ADD PRIMARY KEY (`id_notifica`),
  ADD KEY `Notifica_evento_foreign_key` (`ID_evento`);

--
-- Indici per le tabelle `notifiche_utenti`
--
ALTER TABLE `notifiche_utenti`
  ADD PRIMARY KEY (`id_notifica`,`username`),
  ADD UNIQUE KEY `Notifiche_foreign_key` (`id_notifica`),
  ADD KEY `Utenti_foreign_key` (`username`);

--
-- Indici per le tabelle `tipologie_biglietto`
--
ALTER TABLE `tipologie_biglietto`
  ADD PRIMARY KEY (`ID_biglietto`),
  ADD KEY `Evento_foreign_key` (`ID_evento`);

--
-- Indici per le tabelle `utenti`
--
ALTER TABLE `utenti`
  ADD PRIMARY KEY (`username`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `eventi`
--
ALTER TABLE `eventi`
  MODIFY `ID_evento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT per la tabella `luoghi`
--
ALTER TABLE `luoghi`
  MODIFY `ID_luogo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT per la tabella `notifiche`
--
ALTER TABLE `notifiche`
  MODIFY `id_notifica` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `tipologie_biglietto`
--
ALTER TABLE `tipologie_biglietto`
  MODIFY `ID_biglietto` int(11) NOT NULL AUTO_INCREMENT;

--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `acquisti_utenti`
--
ALTER TABLE `acquisti_utenti`
  ADD CONSTRAINT `Biglietti_foreign_key` FOREIGN KEY (`ID_biglietto`) REFERENCES `tipologie_biglietto` (`ID_biglietto`),
  ADD CONSTRAINT `Utenti_acquisto_foreign_key` FOREIGN KEY (`username`) REFERENCES `utenti` (`username`);

--
-- Limiti per la tabella `eventi`
--
ALTER TABLE `eventi`
  ADD CONSTRAINT `Creatore_foreign_key` FOREIGN KEY (`Username_creatore`) REFERENCES `utenti` (`username`),
  ADD CONSTRAINT `Luoghi_foreign_key` FOREIGN KEY (`ID_luogo`) REFERENCES `luoghi` (`ID_luogo`);

--
-- Limiti per la tabella `notifiche`
--
ALTER TABLE `notifiche`
  ADD CONSTRAINT `Notifica_evento_foreign_key` FOREIGN KEY (`ID_evento`) REFERENCES `eventi` (`ID_evento`);

--
-- Limiti per la tabella `notifiche_utenti`
--
ALTER TABLE `notifiche_utenti`
  ADD CONSTRAINT `Notifiche_foreign_key` FOREIGN KEY (`id_notifica`) REFERENCES `notifiche` (`id_notifica`),
  ADD CONSTRAINT `Utenti_foreign_key` FOREIGN KEY (`username`) REFERENCES `utenti` (`username`);

--
-- Limiti per la tabella `tipologie_biglietto`
--
ALTER TABLE `tipologie_biglietto`
  ADD CONSTRAINT `Evento_foreign_key` FOREIGN KEY (`ID_evento`) REFERENCES `eventi` (`ID_evento`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
