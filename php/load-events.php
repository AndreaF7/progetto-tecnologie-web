<?php if(!isset($templateParams["eventi"]) || count($templateParams["eventi"]) === 0): ?>
    <p class="text-center font-bold">
        Eventi non presenti
    </p>
<?php endif;
require_once 'bootstrap.php';
$lastArrayKey = end($templateParams["eventi"])["ID_evento"];
foreach($templateParams["eventi"] as $evento): ?>
    <section>   <!--Evento singolo-->
    <header>
        <div class="row">
            <div class="col-lg-5 text-lg-left">
            <h3 class="font-weight-bold">
                <em class = "fas fa-calendar-alt"></em>
                <?php echo date("d/m/Y",strtotime($evento["data_evento"])); ?>
            </h3>
            </div>    
            <div class="col-lg-7">
                <h3 class="font-weight-bold"><?php echo $evento["Titolo"]; ?></h3>
            </div>
        </div>
    </header>
    <main>
        <div class="row">
            <div class="col-lg-5 text-center text-lg-left event-img">
                <img class="img-fluid" src="<?php echo UPLOAD. $evento["Immagine"]; ?>" alt="Immagine evento" />
            </div>
            <div class="col-lg-7">
                <div class="row mb-3">
                    <div class="col-xl-10 col-md-11">
                        <p class="text-justify"><?php echo substr($evento["Testo"],0,100) . "..."; ?></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid text-right h4">
            <a class="badge badge-light p-3" href="evento-singolo.php?id_evento=<?php echo $evento["ID_evento"]; ?>">Vedi informazioni</a>
        </div>
    </main>
</section>
<?php if($lastArrayKey != $evento["ID_evento"]): ?>
<hr/>
<?php endif; ?>
<?php endforeach; ?>