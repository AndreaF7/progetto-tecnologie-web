<!DOCTYPE html>
<html lang="it">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
        <title>AGE - <?php echo $templateParams["pageName"]; ?></title>
        <link rel="stylesheet" type="text/css" href="css/style.css" />
        <script src="js/search-bar-script.js" type="text/javascript"></script>
        <script src="js/events_list.js" type="text/javascript"></script>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/Wruczek/Bootstrap-Cookie-Alert@gh-pages/cookiealert.css">
    </head>
    <body>
    <?php
        //Molto probabilmente si può fare in un altro modo
        if(isset($templateParams["cookies"]) && $templateParams["cookies"] == true):
    ?>
        <!-- START Bootstrap-Cookie-Alert -->
        <div class="alert text-center cookiealert" role="alert">
            <em>Do you like cookies?</em> &#x1F36A; We use cookies to ensure you get the best experience on our website. <a href="https://cookiesandyou.com/" target="_blank">Learn more</a>
        
            <button type="button" class="btn btn-primary btn-sm acceptcookies mt-5 mb-5" aria-label="Close">
                I agree
            </button>
        </div>
        <!-- END Bootstrap-Cookie-Alert -->
    <?php
        endif;
        $templateParams["cookies"] = false;
    ?>
    <script src="https://cdn.jsdelivr.net/gh/Wruczek/Bootstrap-Cookie-Alert@gh-pages/cookiealert.js"></script>
        <div class="container-fluid bg-text position-relative">
            <header>
                <nav class="navbar navbar-expand-lg navbar-primary mb-5">
                    <div class="container navbar-brand m-0" id="containerLogo">
                        <a class="text-white" href="index.php">
                            <img class="w-100" src="logo/logo.png" alt="Pagina principale di AGE" />
                        </a>
                    </div>
                    <button class="navbar-toggler text-white" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                        <em class="fas fa-bars fa-2x"></em>
                    </button>
                    <div class="collapse navbar-collapse" id="collapsibleNavbar">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item" id="searchLable">
                                <a class="nav-link text-white search-button pr-3" href="#">
                                    <label class="mr-1 mb-0" for="textSearch">Ricerca</label>
                                    <em class="fas fa-search"></em>
                                </a>
                                <form class="form-inline h-100" action="ricerca.php" method="GET">
                                    <input type="search" name="textSearch" id="textSearch" class="search-bar" placeholder="Search..." />
                                </form>             
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-white" href="info.php">Chi siamo</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-white" href="carrello.php">Carrello <em class="fas fa-shopping-cart"></em></a>
                            </li>
                            <?php
                            if(!empty($_SESSION["tipo"]) && ($_SESSION["tipo"] == "O" || $_SESSION["tipo"] == "A")){
                                echo '
                                <li class="nav-item">
                                    <a class="nav-link text-white" href="eventi.php">Eventi <em class="fas fa-calendar-day"></em></a>
                                </li>';
                            }
                            if(!empty($_SESSION["username"])){
                                echo '
                                <li class="nav-item">
                                    <a class="nav-link text-white" href="notifiche.php">Notifiche <em class="fas fa-envelope"></em></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link text-white" href="profile.php">Profilo <em class="fas fa-user"></em></a>
                                </li>';
                            }else{
                                echo '
                                <li class="nav-item">
                                    <a class="nav-link text-white" href="login.php">Accedi <em class="fas fa-user"></em></a>
                                </li>';
                            }
                            ?>
                        </ul>
                    </div>  
                </nav>   
            </header>
            <main class="text-center">
                <?php 
                    if(isset($templateParams["nome"])){
                        require($templateParams["nome"]);
                    }
                ?>
            </main>
            <footer class="w-100">
                    <p class="m-0 p-3">AGE.COM
                        <br/>Email: infoage_techweb@gmail.com
                        <br/>Telefono: 051 209 9111
                    </p> 
            </footer>
        </div>
    </body>
</html> 