<?php if(isset($templateParams["eventi"]) && !empty($templateParams["eventi"])): 
    $lastArrayKey = end($templateParams["eventi"])["ID_evento"];
?>
    <?php foreach($templateParams["eventi"] as $evento): ?>
    <section class="w-75 mx-auto">
        <header>
            <div class="row mb-2">
                <div class="col-lg-5 text-lg-left">
                    <h3 class="font-weight-bold">
                        <em class = "fas fa-calendar-alt"></em>
                        <?php echo date("d/m/Y",strtotime($evento["data_evento"])); ?>
                    </h3>
                    <p>
                        <em class="fas fa-map-pin"></em>
                        <text>
                            <?php echo $evento["nome_luogo"]; ?>
                        </text>
                    </p>
                </div>    
                <div class="col-lg-7">
                    <h3 class="font-weight-bold"><?php echo $evento["Titolo"]; ?></h3>
                </div>
            </div>
        </header>
        <main class="p-0">
            <div class="row">
                <div class="col-lg-5 text-center text-lg-left event-img">
                    <img class="img-fluid" src="<?php echo UPLOAD . $evento["Immagine"]; ?>" alt="Immagine evento: <?php echo $evento["Titolo"]; ?>" />
                </div>
                <div class="col-lg-7">
                    <div class="row mb-3">
                        <div class="col-xl-10 col-md-11">
                            <p class="text-justify">
                                <?php echo substr($evento["Testo"],0,100) . "..."; ?>
                            </p>   
                        </div>
                    </div>
                </div>
            </div>
            <form method="get" action="evento-singolo.php">
                <div class="container-fluid text-right h4">
                    <a class="badge badge-light p-3" href="evento-singolo.php?id_evento=<?php echo $evento["ID_evento"]; ?>">Vedi informazioni</a>
                </div>
            </form>
        </main>
        <?php if($lastArrayKey != $evento["ID_evento"]): ?>
        <hr/>
        <?php endif; ?>
    </section>
    <?php endforeach; ?>
    <?php if(isset($templateParams["other"]) && $templateParams["other"] === true): ?>
        <div class="container-fluid h4 py-4">
            <a class="btn badge badge-light py-3 px-5 cursor-pointer" onclick="reloadPage()">Vedi altro</a>
        </div>
    <?php endif; ?>
<?php else: ?>
    <em class="far fa-frown fa-9x pb-3"></em>
    <p class="text-center font-bold">
        Eventi non presenti
    </p>
<?php endif; ?>