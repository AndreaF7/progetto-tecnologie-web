<?php $disponibili = $templateParams["evento"][0]["max_biglietti"] - $templateParams["evento"][0]["comprati"];?>
<section class="w-75 mx-auto">
        <header>
            <div class="row mb-2">  
                <div class="col-lg-7">
                    <h1 class="font-weight-bold"><?php echo $templateParams["evento"][0]["Titolo"]; ?></h1>
                </div>
            </div>
        </header>
        <main class="p-0">
            <div class="row">
                <div class="col-lg-5 text-center text-lg-left event-img">
                    <img class="img-fluid" src="<?php echo UPLOAD . $templateParams["evento"][0]["Immagine"]; ?>" alt="Immagine evento" />
                </div>
                <div class="col-lg-7">
                    <div class="row">
                        <div class="col-lg-7">
                            <p class="text-justify font-weight-bold">
                                <em class = "fas fa-calendar-alt"></em>
                                <?php echo date("d/m/Y", strtotime($templateParams["evento"][0]["data_evento"])); ?>
                            </p>
                        </div> 
                    </div>  
                    <div class="row">
                        <div class="col-lg-7">
                            <p class="text-justify">
                                <em class="fas fa-map-pin"></em>
                                <text>
                                    <?php echo $templateParams["evento"][0]["nome_luogo"] . " - " . $templateParams["evento"][0]["indirizzo"]. ", " . $templateParams["evento"][0]["città"]; ?>
                                </text>
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <?php if($disponibili <= 0): ?>
                                <p class="text-justify font-weight-bold">SOLD OUT</p>
                            <?php else: ?>
                                <p class="text-justify font-weight-bold">Biglietti disponibili: <?php echo $disponibili; ?></p>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <p class="text-justify font-weight-bold">
                            Prezzo: 
                            <?php 
                            if($templateParams["evento"][0]["prezzo"] == 0){
                                echo "GRATUITO";
                            } else {
                                echo $templateParams["evento"][0]["prezzo"] . "€"; 
                            }
                            ?>
                            </p>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-xl-10 col-md-11">
                            <p class="text-justify">
                                <?php echo $templateParams["evento"][0]["Testo"]; ?>
                            </p>   
                        </div>
                    </div>
                </div>
            </div>  
            <div class="container-fluid text-right mb-3 h4">
<?php
    if(isset($_SESSION["tipo"]) && isset($_SESSION["username"])):
        if(($_SESSION["tipo"] == "O" && $_SESSION["username"] == $templateParams["evento"][0]["Username_creatore"]) || $_SESSION["tipo"] == "A"):
?>
                <a class="btn badge p-3 m-3 btn-blue" href="modifica-evento.php?id_evento=<?php echo $templateParams["evento"][0]["ID_evento"]; ?>">Modifica evento</a>
<?php   
        endif; 
    endif;
?>
<?php if($disponibili > 0): ?>
                <a class="btn badge badge-light p-3 m-3
                    <?php 
                        if(isset($_COOKIE["carrello"]) && in_array($templateParams["evento"][0]["ID_evento"], unserialize($_COOKIE["carrello"], ["allowed_classes" => false]))){
                            echo "disabled";
                        }
                    ?> addToChart" 
                    onclick="<?php echo "addEvent(" . $templateParams["evento"][0]["ID_evento"] . ")"; ?>">Aggiungi al carrello</a>
<?php endif; ?>
            </div>  
        </main>
</section>
