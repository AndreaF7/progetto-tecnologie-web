<?php
    if(isset($templateParams["notifiche"]) && !empty($templateParams["notifiche"])):
        $lastArrayKey = end($templateParams["notifiche"])["id_notifica"];
?>
    <?php 
        foreach($templateParams["notifiche"] as $notifica):
    ?>        
        <section class="w-75 mx-auto">
            <header>
                <div class="row mb-2">
                    <div class="text-lg-left">
                        <h2 class="w-100 font-weight-bold">
                            <em class="far fa-envelope-open"></em>
                            <?php echo $notifica["Titolo"]; ?>
                        </h2>
                    </div>
                </div>
            </header>
            <main class="p-0 mb-5">
                <div class="container-fluid bg-light rounded-lg border border-dark mb-2">
                    <p class="text-justify">
                        <?php echo $notifica["Testo"]; ?>
                    </p> 
                </div> 
                <?php
                    if($notifica["ID_evento"] != null):
                ?>
                <div class="container-fluid text-right h4">
                    <a class="badge badge-light p-3" href="evento-singolo.php?id_evento=<?php echo $notifica["ID_evento"]; ?>">Pagina evento</a>
                </div>
                <?php
                    endif;
                ?>
            </main>
            <?php if($lastArrayKey != $notifica["id_notifica"]): ?>
            <hr/>
            <?php endif; ?>
        </section>
    <?php
        endforeach;
    ?>
<?php
    else:
?>
    <em class="far fa-envelope-open fa-9x pb-3"></em>
    <p class="text-center font-bold">
        Non ci sono notifiche...
    </p>
<?php
    endif;
?>