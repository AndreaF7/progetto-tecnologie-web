<?php if(isset($templateParams["eventi_carrello"]) && !empty($templateParams["eventi_carrello"])): 
    $lastArrayKey = end($templateParams["eventi_carrello"])["ID_evento"];
?>
    <form action="acquisto_completato.php" method="POST">
        <?php foreach($templateParams["eventi_carrello"] as $evento): ?>
        <section class="w-75 mx-auto">
            <header>
                <div class="row mb-2">
                    <div class="col-lg-5 text-lg-left">
                        <h3 class="font-weight-bold">
                            <em class = "fas fa-calendar-alt"></em>
                            <?php echo date("d/m/Y", strtotime($evento["data_evento"])); ?>
                        </h3>
                    </div>    
                    <div class="col-lg-7">
                        <h3 class="font-weight-bold"><?php echo $evento["Titolo"]; ?></h3>
                    </div>
                </div>
            </header>
            <main class="p-0">
                <div class="row">
                    <div class="col-lg-5 text-center text-lg-left event-img">
                        <img class="img-fluid" src="<?php echo UPLOAD . $evento["Immagine"]; ?>" alt="Immagine evento <?php echo $evento["Titolo"]; ?>" />
                    </div>
                    <div class="col-lg-7">
                        <div class="row mb-3">
                            <div class="col-xl-10 col-md-11">
                                <p class="text-justify">
                                    <?php echo substr($evento["Testo"],0,100) . "..."; ?>
                                </p>   
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container-fluid text-right h4 pb-5">
                    <label class="m-0 mr-3 align-bottom">Prezzo biglietto singolo: <?php echo $evento["prezzo"]?> €</label>
                    <?php $disponibili = $evento["max_biglietti"] - $evento["comprati"];?>
                    <label for="disponibilita<?php echo $evento["ID_evento"]; ?>" hidden>Numero biglietti da acquistare</label>
                    <select class="form-control w-25 select-right" name="disponibilita<?php echo $evento["ID_evento"]; ?>" id="disponibilita<?php echo $evento["ID_evento"]; ?>">
                        <?php 
                                for($i = 1; $i <= $disponibili; $i++)
                                echo "<option value='" . $i . "'>" . $i . "</option>";
                        ?>
                    </select>
                </div>
                <div class="container-fluid text-right h4">
                    <a class="badge badge-light p-3" href="evento-singolo.php?id_evento=<?php echo $evento["ID_evento"]; ?>">Vedi informazioni</a>
                    <a class="btn badge badge-danger text-white p-3 m-3" onclick="<?php echo "removeEvent(" . $evento["ID_evento"] . ")"; ?>">Rimuovi</a>
                </div>
            </main>
            <?php if($lastArrayKey != $evento["ID_evento"]): ?>
            <hr/>
            <?php endif; ?>
        </section>
        <?php endforeach; 
        if(empty($_SESSION["username"])){
        echo '<div class="container-fluid h1">
                <a id="buy-btn" class="btn badge submit-button text-white p-3 m-3" href="login.php">Acquista</a>
            </div>';
        } else {
        echo '<div class="container-fluid h1">
                <button id="buy-btn" type="submit" class="btn badge submit-button text-white p-3 m-3">Acquista</a>
            </div>';
        } ?>
    </form>
<?php else: ?>
    <em class="far fa-frown fa-9x pb-3"></em>
    <p class="text-center font-bold">
        Carrello vuoto...
    </p>
<?php endif; ?>