<?php
require_once 'bootstrap.php';

if(isset($_POST["email"]) && isset($_POST["password"])){
    $login_result = $dbh->checkLogin($_POST["email"],$_POST["password"]);
    if(count($login_result) == 0){
        $wrong_data = 1;
    } else{     //register current user session
        $user = $login_result[0];
        $_SESSION["username"] = $user["username"];
        $_SESSION["nome"] = $user["nome"];
        $_SESSION["tipo"] = $user["tipo"];
        header("Location: index.php");
    }
}
?>
<!DOCTYPE html>
<html lang="it">
    <head>
        <meta name="viewport" charset="utf-8" content="width=device-width, initial-scale=1.0" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
        <title>AGE - Accedi</title>
        <link rel="stylesheet" type="text/css" href="css/style.css" />
    </head>
    <body class="body-full-bg">
        <?php
        if(isset($wrong_data) && $wrong_data == 1){
            echo '<script type="text/javascript">

            window.onload = function () { alert("Dati errati!"); }
  
            </script>';
        }
        ?>
        <header class="text-center">            <!--TODO: make id for the logo in a way that it sizes with the page-->
            <a href="index.php"><img class="logo-img" src="logo/logo.png" alt="logo" /></a>
        </header>
        <main>
            <h1 class="welcome-text">Benvenuto</h1>
            <div id="loginForm" class="card col-10 row-8 col-lg-5">   <!--Parte centrale rettangolo bianco-->
                <div class="card-body">
                    <form method="POST" action="#">
                        <div class="input-group form-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><em class="fas fa-user"></em></span>
                            </div>
                            <label for="email" hidden>Indirizzo email</label>
                            <input type="text" class="form-control" name="email" id="email" placeholder="Indirizzo Email" required/>
                        </div>
                        <div class="input-group form-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><em class="fas fa-key"></em></span>
                            </div>
                            <label for="password" hidden>Password</label>
                            <input type="password" class="form-control" name="password" id="password" placeholder="Password" required/>
                        </div>
                        <div class="form-group">
                            <label for="enter" hidden>Invio</label>
                            <input type="submit" id="enter" value="Entra" class="btn float-right login_btn">
                        </div>
                    </form>
                </div>
                <div class="card-footer">
                    <div class="d-flex justify-content-center links">
                        Non hai un account?<a class="register-link" href="registrazione.php">Registrati</a>
                    </div>
                </div>  
            </div>
        </main>
    </body>
</html>