<?php 
require_once("bootstrap.php");
require("utils/functions.php");
require("utils/mailSetter.php");

$templateParams["pageName"] = "Acquisto completato";
$templateParams["nome"] = "template/acquisto.php";
$IDEvents = unserialize($_COOKIE["carrello"], ["allowed_classes" => false]);
$msg = "<h3>Il team di AGE.com ti ringrazia di aver acquistato i biglietti dei seguenti eventi:</h3>\n
        <ul>\n";
foreach($IDEvents as $ID){
    $event = $dbh->getEventByID($ID)[0];
    $dbh->addBoughtEventTickets($ID, $_SESSION["username"], $_POST["disponibilita" . $ID]);
    $prezzo = $event["prezzo"] * $_POST["disponibilita" . $ID];
    $msg = $msg . "<li>" . $event["Titolo"] . " - numero biglietti: " . $_POST["disponibilita" . $ID] . " - prezzo: " . $prezzo . " euro</li>\n";
    $biglietti_restanti = $event["max_biglietti"] - $event["comprati"] - $_POST["disponibilita" . $ID];
    //Send notification to event's creator, if it's sold out
    if($biglietti_restanti == 0){
        $msgSoldOut = "<h1>Congratulazioni! :)</h1>\n
                        <p>Sono stati venduti tutti i biglietti dell'evento '" . $event["Titolo"] . "' da te creato.</p>";
        sendNotification($event["Username_creatore"], $dbh->getUserByID($event["Username_creatore"])[0]["email"], "SOLDOUT il suo evento '" . $event["Titolo"] . "'",
                    $msgSoldOut, $ID);
        /*
        $mail = newMail();
        $mail->Subject = "SOLDOUT il suo evento '" . $event["Titolo"] . "'";
        $mail->AddAddress($dbh->getUserByID($event["Username_creatore"])[0]["email"]);
        $mail->Body = "<h1>Congratulazioni! :)</h1>\n
                        <p>Sono stati venduti tutti i biglietti dell'evento '" . $event["Titolo"] . "' da te creato.</p>";
        $mail->send();
        */
    }
}
$msg = $msg . "</ul>";
sendNotification($_SESSION["username"], $dbh->getUserByID($_SESSION["username"])[0]["email"], "Riepilogo acquisti AGE.com",
                    $msg, null);
/*
$mail = newMail();
$mail->Subject = "Riepilogo acquisti AGE.com";
$mail->AddAddress($dbh->getUserByID($_SESSION["username"])[0]["email"]);
$mail->Body = $msg;
$mail->send();
*/

//Unsetting shopping chart
setcookie("carrello", "", time()-3600, "/");
unset($_COOKIE["carrello"]);

require("template/base.php");
?>