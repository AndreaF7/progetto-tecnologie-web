$(document).ready(function(){
    $('#btn-login').click(function(){
        document.location.href="login.php";
    });
    $('.btn-outline-light').click(function(){
        if(!$(this).hasClass("active")){
            $('.btn-outline-light').removeClass("active");
            $(this).addClass("active");
            if($(this).is("#btn-organizzatore")){
                document.getElementById("last-row").innerHTML +=
                `<div class="col-md-6">
                    <div class="form-group">
                        <label class="labelForms" for="iva">Partita IVA: </label>
                        <input type="text" class="form-control" name="iva" id="iva" placeholder="Partita IVA" required/>
                    </div>
                </div>`;
            } else{
                document.getElementById("last-row").innerHTML =
                `<div class="col-md-6">
                    <div class="form-group">
                        <label class="labelForms" for="password2">Conferma password: </label>
                        <input type="password" class="form-control" name="password2" id="password2" placeholder="Conferma password" required/>
                    </div>
                </div>`;
            }
        }
    });
})