function reloadPage(){
    const xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            $("body > div > main").html(this.responseText);
        }
    };
    const nEvents = $("body > div > main > section").length + 5 + "&onlyMain=" + true;
    console.log(nEvents);
    xhttp.open("GET", window.location.href + "&events=" + nEvents, true);
    xhttp.send();
}

function removeEvent(id){
    const xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            $("body > div > main").html(this.responseText);
        }
    };
    const data =  {'removeEvent': id};
    if (confirm("Conferma eliminazione evento")){
        $.post("utils/functions.php", data, function (response) {
            console.log(window.location.href + "?onlyMain=" + true);
            xhttp.open("GET", window.location.href + "?onlyMain=" + true, true);
            xhttp.send();
        });
    }
}

function addEvent(id){
    const data =  {'addEvent': id};
    $.post("utils/functions.php", data, function (response) {
        alert("Evento aggiunto al carrello!");
    });
}

$(document).ready(function(){
    $(".addToChart").click(function(e){
        e.preventDefault();
        if(!$(this).hasClass("disabled")){
            $(this).addClass("disabled");
        }
    })
}
);