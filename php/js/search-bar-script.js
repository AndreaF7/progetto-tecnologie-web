$(document).ready(function(){
    $(".search-bar").hide();
    $(".search-button").click(function(e){
        e.preventDefault();
        $(".search-bar").removeClass("d-none");
        if($(window).width() > 992){
            $("a.search-button > label").fadeOut();
        }
        $(".search-bar").show("slow");
        $(".search-bar").focus();
        $("a.search-button").addClass("disabled");
    });
    $(".search-bar").focusout(function(){
        if(!$(".search-bar").val()){
            $(".search-bar").hide("slow");
            if($(window).width() > 992){
                $("a.search-button > label").fadeIn("slow");
            }
            $("a.search-button").removeClass("disabled");
        }
    });
})