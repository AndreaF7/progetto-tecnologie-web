<div class="container-fluid position-relative text-center">
    <div id="divImgBlurred">
        <img src="upload/band.jpg" id="blurredImg" class="img-fluid" alt=""/>
    </div>
    <div class="d-inline-block">
        <img src="upload/band.jpg" id="bandImage" class="w-75 h-75" alt="Immagine di una band" />
    </div>
    <div id="rectangleForm" class="card col-10 col-md-8 col-lg-3 text-left bg-light">   <!--Parte centrale rettangolo bianco-->
        <div class="card-body bg-light">
            <form action="home-form-search.php" method="GET">
                <div class="form-group">
                    <label class="card-subtitle mb-2 font-weight-bold h3" for="localita">Località</label>
                    <select class="form-control" id="localita" name="localita">
                        <?php
                            foreach($dbh->getZones() as $l){
                                echo "<option value='".$l["città"]."'>".$l["città"]."</option>";
                            }
                        ?>
                    </select>
                </div>
                <div class="form-group">
                    <label class="card-subtitle mb-2 font-weight-bold h3" for="giorno">Giorno</label>
                    <input type="date" class="form-control" id="giorno" name="giorno" required />
                </div>
                <div class="form-group">
                    <label class="card-subtitle mb-2 font-weight-bold h3" for="tipo">Tipo</label>
                    <select class="form-control" id="tipo" name="tipo">
                        <?php
                            foreach($dbh->getTypes() as $t){
                                echo "<option>".$t["Tipologia"]."</option>";
                            }
                        ?>
                    </select>
                </div>
                <div>
                    <button type="submit" class="btn btn-circle btn-orange">
                        <em class="fas fa-arrow-right fa-2x text-white"></em>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="bg-white">
    <ul class="nav nav-pills nav-fill mb-3 mt-5 font-weight-bold" id="pills-tab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Novità</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Più gettonate</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false">Prossimi eventi</a>
        </li>
    </ul>
    <div class="tab-content" id="pills-tabContent">   <!--Parte sotto con eventi-->
        <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
            <?php $templateParams["eventi"] = $dbh->getNewEvents(3);
            require('load-events.php'); ?>
        </div>
        <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
        <?php $templateParams["eventi"] = $dbh->getMostParticipatedEvents(3);
            require('load-events.php'); ?>            
        </div>
        <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-profile-tab">
        <?php $templateParams["eventi"] = $dbh->getNearEvents(3);
            require('load-events.php'); ?>
        </div>
    </div>
</div>