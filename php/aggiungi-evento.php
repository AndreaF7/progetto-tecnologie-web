<?php
require_once 'bootstrap.php';

if (isset($_SESSION["username"]) && isset($_SESSION["tipo"])){

$user=$dbh->getUserByID($_SESSION["username"]);

    if($_SESSION["tipo"] == "O"){   

            if(isset($_POST["addEvent"])){   //crea altra sezione per passsword con altra isset(post)[password]
                echo $_POST["prezzo"];
                $errors = array();
                $titolo = $_POST["titolo"];
                $luogo = $_POST["luogo"];
                $indirizzo = $_POST["indirizzo"];
                $nomeLuogo = $_POST["nomeLuogo"];
                $maxBiglietti = $_POST["maxBiglietti"];
                $prezzo = $_POST["prezzo"];
                $dataEvento = $_POST["dataEvento"];
                $tipologia = $_POST["tipologia"];
                $usernameCreatore = $user[0]["username"] ;
               // echo $_POST["image"];
                $immagine = $_POST["image"];
                $descrizione = $_POST["descrizione"];
                $queryResult = $dbh->getPlace($luogo, $indirizzo);
                if(empty($queryResult)){
                    $queryResult = $dbh->addPlace($nomeLuogo, $luogo, $indirizzo);
                }
                $IDLuogo = $queryResult[0]["ID_luogo"];
                $descrizione = $_POST["descrizione"];
                $immagine = $_POST["image"];
                $dataCreazione = date("Y/m/d");
                if(empty($queryResult)){ array_push($errors, "Errore nel'aggiunta del luogo"); }
                if (empty($titolo)) { array_push($errors, "Titolo richiesto"); }
                if (empty($luogo)) { array_push($errors, "Locazione richiesta"); }
                if (empty($indirizzo)) { array_push($errors, "Indirizzo richiesto"); }
                if (empty($maxBiglietti)) { array_push($errors, "Numero biglietti totali richiesto"); }
                if ($maxBiglietti<=0){ array_push($errors, "Numero biglietti non valido! \nPer favore inserire un numero di biglietti maggiore di 0"); }
            //  if ($password1 != $password2) { array_push($errors, "Le password non sono uguali"); }
                if (empty($dataEvento)) { array_push($errors, "Data evento richiesta"); }
                if (empty($tipologia)) { array_push($errors, "Tipologia evento richiesta"); }
                if (empty($usernameCreatore)) { array_push($errors, "Nome dell'organizzatore richiesto"); }
                if (empty($descrizione)) { array_push($errors, "Descrizione mancante"); }
                if (empty($immagine)) {
                    array_push($errors, "Immagine mancante");
                } else {
                    $target_dir = "upload/";
                    $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
                    $uploadOk = 1;
                    $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
                    // Check if image file is a actual image or fake image
                    if(isset($_POST["addEvent"])) {
                        $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
                        if($check !== false) {
                            echo "File is an image - " . $check["mime"] . ".";
                            $uploadOk = 1;
                        } else {
                            array_push($errors, "File is not an image.");
                            $uploadOk = 0;
                        }
                    }
                    // Check if file already exists
                    if (file_exists($target_file)) {
                        array_push($errors, "Sorry, file already exists.");
                        $uploadOk = 0;
                    }
                    // Check file size
                    if ($_FILES["fileToUpload"]["size"] > 500000) {
                        array_push($errors, "Sorry, your file is too large.");
                        $uploadOk = 0;
                    }
                    // Allow certain file formats
                    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
                    && $imageFileType != "gif" ) {
                        array_push($errors, "Sorry, only JPG, JPEG, PNG & GIF files are allowed.");
                        $uploadOk = 0;
                    }
                    // Check if $uploadOk is set to 0 by an error
                    if ($uploadOk == 0) {
                        array_push($errors, "Sorry, your file was not uploaded.");
                    // if everything is ok, try to upload file
                    } else {
                        if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
                            echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
                        } else {
                            array_push($errors, "Sorry, there was an error uploading your file.");
                        }
                    }
                }


            //  if(isset($_POST["iva"]) && empty($_POST["iva"])){array_push($errors, "Partita iva richiesta");}
                if(count($errors) == 0){
                    $check_result = $dbh->checkEvent($_POST["titolo"], $IDLuogo, $_POST["dataEvento"]);
                    if(count($check_result) != 0){
                            array_push($errors,"Evento già registrato, non è possibile creare eventi duplicati");
                        } else{     //register current user session

                            //TODO: pass data in correct way
                            $dbh->addEvent($titolo, $maxBiglietti, $prezzo, $IDLuogo, $dataEvento, $tipologia, $usernameCreatore, $descrizione, $immagine, $dataCreazione);
                            header("Location: eventi.php");
                    }
                }   
            }          

        }
    } else {
        header("Location: login.php");
}


?>
<!DOCTYPE html>
<html lang="it">
    <head>
        <meta name="viewport" charset="utf-8" content="width=device-width, initial-scale=1.0" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
        <title>AGE - Aggiunta Nuovo Evento</title>
        <link rel="stylesheet" type="text/css" href="css/style.css" />
        <script src="js/upload_image.js" type="text/javascript"></script>
    </head>
    <body class="body-full-bg">
        <?php
        if(isset($errors)){
            echo "<script type='text/javascript'>

            window.onload = function () { alert(".json_encode($errors)."); }
  
            </script>";
        }
        ?>
        <header class="text-center">            <!--TODO: make id for the logo in a way that it sizes with the page-->
            <a href="index.php"><img class="logo-img" src="logo/logo.png" alt="logo" /></a>
        </header>
        <main>
            <div class="container register-form">    <!--usa questo div per l'altro form-->
                <form method="POST" action="#" enctype="multipart/form-data">
                    <div class="form">
                        <div class="form-content">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label id="label_eventTitle" class="labelForms" for="eventTitle">Titolo:</label>
                                        <input type="text" class="form-control" id="eventTitle" placeholder="Titolo Evento" name="titolo" required/>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label id="label_creatorUsername" class="labelForms" for="creatorUsername">Nome Creatore:</label>
                                        <input type="text" class="form-control" id="creatorUsername" value="<?php echo $user[0]["username"]; ?>" name="usernameCreatore" readonly/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label id="label_dataEvento" class="labelForms" for="date">Data Evento:</label>
                                                <input class="form-control" id="date" name="dataEvento" type="date" required/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label id="label_eventType" class="labelForms" for="eventType">Tipo Evento:</label>
                                        <input type="text" class="form-control" id="eventType" name="tipologia" placeholder="Tipologia Evento" required/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label id="label_ticket" class="labelForms" for="ticket">Biglietti Disponibili: </label>
                                                <input type="number" min="1" class="form-control" id="ticket" name="maxBiglietti" type="number" placeholder="Numero Biglietti" required/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label id="label_ticketPrice" class="labelForms" for="ticketPrice">Prezzo Biglietti:</label>
                                        <input type="number" min="0" step="0.01" class="form-control" id="ticketPrice" name="prezzo" placeholder="Prezzo Singolo Biglietto" required/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label id="label_eventCity" class="labelForms" for="eventCity">Città Evento:</label>
                                        <input type="text" class="form-control" id="eventCity" placeholder="Luogo Evento (es. Cesena, Milano, ...)" name="luogo" required/>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label id="label_locationAddress" class="labelForms" for="locationAddress">Indirizzo Location:</label>
                                        <input type="text" class="form-control" id="locationAddress" placeholder="Indirizzo" name="indirizzo" required/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label id="label_fileToUpload" class="labelForms" for="fileToUpload">Immagine:</label>
                                    <input type="file" name="fileToUpload" class="file" id="fileToUpload"/>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1"><em class="fas fa-paperclip"></em></span>
                                            </div>
                                            <label for="image" hidden>Percorso immagine</label>
                                            <input type="text" name="image" id="image" class="form-control" placeholder="Upload File" aria-label="Upload File" aria-describedby="basic-addon1"/>
                                            <div class="input-group-append">
                                                <button type="button" class="browse input-group-text btn btn-warning" id="basic-addon2"><em class="fas fa-search"></em>  Browse</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label id="label_eventLocation" class="labelForms" for="eventLocation">Location Evento:</label>
                                        <input type="text" class="form-control" id="eventLocation" placeholder="Location (es. Discoteca Barracuda)" name="nomeLuogo" required/> 
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label id="label_eventDescription" class="labelForms" for="eventDescription">Descrizione Evento:</label>
                                        <textarea class="form-control" id="eventDescription" placeholder="Descrizione Evento" name="descrizione" cols="30" rows="10" required></textarea>
                                    </div>
                                </div>
                            </div>
                            <label for="modifyEvent" hidden>Modifica Evento</label>
                            <input type="submit" value="Aggiungi Evento" class="btn float-right btn-info" name="addEvent" />
                        </div>
                    </div>
                </form>
            </div>
        </main>
    </body>
</html>