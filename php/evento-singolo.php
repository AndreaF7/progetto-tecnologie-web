<?php 
    require_once "bootstrap.php";
    
    $idEvento = $_GET["id_evento"];
    $templateParams["pageName"] = "Evento";
    $templateParams["nome"] = "template/evento.php";
    $templateParams["evento"] = $dbh->getEventByID($idEvento); 
    
    require "template/base.php";
?>
