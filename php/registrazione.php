<?php
require_once 'bootstrap.php';

if(isset($_POST["reg"])){
    $errors = array();
    $username = $_POST["username"];
    $email = $_POST["email"];
    $password1 = $_POST["password1"];
    $password2 = $_POST["password2"];
    $nome = $_POST["nome"];
    $cognome = $_POST["cognome"];
    $data = $_POST["data"];
    if (empty($username)) { array_push($errors, "Username richiesto"); }
    if (empty($email)) { array_push($errors, "Email richiesta"); }
    if (empty($password1)) { array_push($errors, "Password richiesta"); }
    if ($password1 != $password2) { array_push($errors, "Le password non sono uguali"); }
    if (empty($nome)) { array_push($errors, "Nome richiesto"); }
    if (empty($cognome)) { array_push($errors, "Cognome richiesto"); }
    if (empty($data)) { array_push($errors, "Data richiesta"); }
    if(isset($_POST["iva"]) && empty($_POST["iva"])){array_push($errors, "Partita iva richiesta");}
    if(count($errors) == 0){
        $check_result = $dbh->checkUser($_POST["username"],$_POST["email"]);
        if(count($check_result) != 0){
            if($check_result[0]["username"] == $username){
                array_push($errors,"Username già presente");
            }
            if($check_result[0]["email"] == $email){
                array_push($errors,"Email già presente");
            }
        } else{     //register current user session
            if(isset($_POST["iva"])){
                $iva = $_POST["iva"];
                $tipo = "O";
            }else{
                $tipo = "U";
            }
            //TODO: pass data in correct way
            $dbh->registerUser($username,$nome,$cognome,$data,$email,$password1,$iva,$tipo);
            $_SESSION["username"] = $_POST["username"];
            $_SESSION["nome"] = $_POST["nome"];
            $_SESSION["tipo"] = $tipo;
            header("Location: index.php");
        }
    }
}
?>
<!DOCTYPE html>
<html lang="it">
    <head>
        <meta name="viewport" charset="utf-8" content="width=device-width, initial-scale=1.0" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
        <title>AGE - Registrazione</title>
        <link rel="stylesheet" type="text/css" href="css/style.css" />
        <script src="js/register-buttons.js" type="text/javascript"></script>
    </head>
    <body class="body-full-bg">
        <?php
        if(isset($errors)){
            echo "<script type='text/javascript'>

            window.onload = function () { alert(".json_encode($errors)."); }
  
            </script>";
        }
        ?>
        <header class="text-center">            <!--TODO: make id for the logo in a way that it sizes with the page-->
            <a href="index.php"><img class="logo-img" src="logo/logo.png" alt="logo" /></a>
        </header>
        <main>
            <div class="container register-form">
                <form method="POST" action="#">
                    <div class="form">
                        <div class="btn-group" role="group">
                            <button type="button" id="btn-cliente" class="btn btn-outline-light active">Cliente</button>
                            <button type="button" id="btn-organizzatore" class="btn btn-outline-light">Organizzatore</button>
                        </div>
                        <button type="button" id="btn-login" class="btn btn-outline-light float-right">Entra</button>
                        <div class="form-content">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="labelForms" for="nome">Nome: </label>
                                        <input type="text" class="form-control" placeholder="Nome" name="nome" id="nome" required/>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="labelForms" for="cognome">Cognome: </label>
                                        <input type="text" class="form-control" placeholder="Cognome" name="cognome" id="cognome" required/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label id="date-label" class="labelForms" for="data">Data di nascita:</label>
                                                <input class="form-control" id="data" name="data" type="date" required/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="labelForms" for="username">Username: </label>
                                        <input type="text" class="form-control" id="username" name="username" placeholder="Username" required/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="labelForms" for="email">Email: </label>
                                        <input type="email" class="form-control" id="email" name="email" placeholder="Indirizzo Email" required/>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="labelForms" for="password1">Password: </label>
                                        <input type="password" class="form-control" name="password1" id="password1" placeholder="Password" required/>
                                    </div>
                                </div>
                            </div>
                            <div class="row" id="last-row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="labelForms" for="password2">Conferma password: </label>
                                        <input type="password" class="form-control" name="password2" id="password2" placeholder="Conferma password" required/>
                                    </div>
                                </div>
                            </div>
                            <label for="reg" hidden>Registrati</label>
                            <input type="submit" value="Registrati" class="btn float-right login_btn" name="reg" id="reg" />
                        </div>
                    </div>
                </form>
            </div>
        </main>
    </body>
</html>