<?php
require_once 'bootstrap.php';
$templateParams["pageName"] = "Ricerca";
$templateParams["nome"] = "template/lista_ricerca.php";
if(!isset($_GET["events"]) || $_GET["events"] === ""){
    $limit = 1;
} else {
    $limit = $_GET["events"];
}
$templateParams["eventi"] = $dbh->getEventsOnHome($_GET["localita"],$_GET["giorno"],$_GET["tipo"],$limit);
if(count($templateParams["eventi"]) < count($dbh->getEventsOnHome($_GET["localita"],$_GET["giorno"],$_GET["tipo"],$limit + 1))){
    $templateParams["other"] = true;
} else {
    $templateParams["other"] = false;
}

if(!isset($_GET["onlyMain"]) || $_GET["onlyMain"] !== "true"){
    require 'template/base.php';
} else {
    require "template/lista_ricerca.php";
}
?>