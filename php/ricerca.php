<?php
require_once("bootstrap.php");
$templateParams["pageName"] = "Ricerca";
$templateParams["nome"] = "template/lista_ricerca.php";
if(isset($_GET["textSearch"]) && $_GET["textSearch"] !== ""){
    if(!isset($_GET["events"]) || $_GET["events"] === ""){
        $limit = 5;
    } else {
        $limit = $_GET["events"];
    }
    $templateParams["eventi"] = $dbh->getEventsByTextSearch($_GET["textSearch"], $limit);
    if(count($templateParams["eventi"]) < count($dbh->getEventsByTextSearch($_GET["textSearch"], $limit + 1))){
        $templateParams["other"] = true;
    } else {
        $templateParams["other"] = false;
    }
} else {
    $templateParams["eventi"] = "";
}

if(!isset($_GET["onlyMain"]) || $_GET["onlyMain"] !== "true"){
    require("template/base.php");
} else {
    require("template/lista_ricerca.php");
}
?>