<?php
require_once("bootstrap.php");
$templateParams["pageName"] = "Notifiche";
$templateParams["nome"] = "template/lista_notifiche.php";
$templateParams["notifiche"] = $dbh->getNotificationsByID($_SESSION["username"]);

require("template/base.php");
?>