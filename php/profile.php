<?php
require_once 'bootstrap.php';

if (isset($_SESSION["username"])){
    $success=0;
    $user = $dbh->getUserByID($_SESSION["username"]);

    if(isset($_POST["oldPasswordProfile"]) && isset($_POST["confirmPasswordProfile"]) && isset($_POST["newPasswordProfile"])) {
        $errors = array();
        $oldPasswordProfile = $_POST["oldPasswordProfile"];
        $confirmPasswordProfile = $_POST["confirmPasswordProfile"];
        $newPasswordProfile = $_POST["newPasswordProfile"];

        if (empty($oldPasswordProfile)) { array_push($errors, "Password richiesta"); }
        if (empty($newPasswordProfile)) { array_push($errors, "Password richiesta"); }
        if (empty($confirmPasswordProfile)) { array_push($errors, "Password richiesta"); }
        if ($newPasswordProfile != $confirmPasswordProfile) { array_push($errors, "Le password non sono uguali"); }
        if(empty($errors)){
            $modifyResult = $dbh->modifyPassword($_SESSION["username"], $oldPasswordProfile, $newPasswordProfile);
            if($modifyResult == 1){
                array_push($errors, "Password errata!");
            }else{
                $success=1;
            }
        }
    } 
}else{
    header("Location: login.php");
}


?>
<!DOCTYPE html>
<html lang="it">
    <head>
        <meta name="viewport" charset="utf-8" content="width=device-width, initial-scale=1.0" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
        <title>AGE - Profilo Utente</title>
        <link rel="stylesheet" type="text/css" href="css/style.css" />
        <script src="js/register-buttons.js" type="text/javascript"></script>
    </head>
    <body class="body-full-bg">
        <?php
        if(!empty($errors)){
            echo "<script type='text/javascript'>

            window.onload = function () { alert(".json_encode($errors)."); }
  
            </script>";
        }if($success==1){
            echo "<script type='text/javascript'>

            window.onload = function () { alert('Password cambiata!'); }
  
            </script>";
        }
        ?>
        <header class="text-center">  
            <div class="container-fluid">
                <a href="index.php"><img class="logo-img" src="logo/logo.png" alt="logo" /></a>
            </div>          <!--TODO: make id for the logo in a way that it sizes with the page-->
        </header>
        <main>
            <div class="container register-form">    <!--usa questo div per l'altro form-->
                <form method="POST" action="#">
                    <div class="form">
                        <div class="form-content">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label id="label_nameProfile" class="labelForms" for="nameProfile">Nome: </label>
                                        <input type="text" class="form-control" placeholder="Nome" name="nameProfile" id="nameProfile" value="<?php echo $user[0]["nome"];?>" readonly/>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label id="label_surnameProfile" class="labelForms" for="surnameProfile">Cognome: </label>
                                        <input type="text" class="form-control" placeholder="Cognome" name="surnameProfile" id="surnameProfile" value="<?php echo $user[0]["cognome"];?>" readonly/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label id="label_dateProfile" class="labelForms" for="date">Data di nascita:</label>
                                                <input class="form-control" name="date" id="date" type="date" value="<?php echo $user[0]["data_nascita"];?>" readonly/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label id="label_usernameProfile" class="labelForms" for="usernameProfile">Username: </label>
                                        <input type="text" class="form-control" name="usernameProfile" id="usernameProfile" placeholder="Username" value="<?php echo $user[0]["username"];?>" readonly/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label id="label_emailProfile" class="labelForms" for="emailProfile">E-mail: </label>
                                        <input type="email" class="form-control" name="emailProfile" id="emailProfile" placeholder="Indirizzo E-mail" value="<?php echo $user[0]["email"];?>" readonly/>
                                    </div>
                                </div>
                                <div class="col-md-6 position-relative">
                                    <div class="form-group">
                                        <label for="logout" hidden>Logout</label>       
                                        <button class="btn btn-info float-right buttonForm" type="button" onclick="window.location.href='logout.php'" name="logout" id="logout">
                                            <text class="text-white">Logout</text>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="container register-form pt-0">    <!--usa questo div per l'altro form-->
                <form method="POST" action="#">
                    <div class="form">
                        <div class="form-content">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label id="label_oldPasswordProfile" class="labelForms" for="oldPasswordProfile">Vecchia Password: </label>
                                        <input type="password" class="form-control" name="oldPasswordProfile" id="oldPasswordProfile" placeholder="Vecchia Password" required/>
                                    </div>
                                    <div class="form-group">
                                    <label id="label_newPasswordProfile" class="labelForms" for="newPasswordProfile">Nuova Password: </label>
                                        <input type="password" class="form-control" name="newPasswordProfile" id="newPasswordProfile" placeholder="Nuova Password" required/>
                                    </div>
                                    <div class="form-group">
                                    <label id="label_confirmPasswordProfile" class="labelForms" for="confirmPasswordProfile">Conferma Nuova Password: </label>
                                        <input type="password" class="form-control" name="confirmPasswordProfile" id="confirmPasswordProfile" placeholder="Conferma Nuova Password" required/>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                <label for="savePassword" hidden>Salva Password</label>       
                                    <input type="submit" value="Salva Password" class="btn float-right btn-info" name="savePassword" id="pass" />
                                </div>
                            </div>
                        </div>


                            <!-- sposta tutto questo di sotto nella div per la modifica della paswword -->


                    </div>
                </form>
            </div>
        </main>
    </body>
</html>