<?php
require_once("bootstrap.php");
$templateParams["eventi_carrello"] = array();
if(isset($_COOKIE["carrello"])) {
    $ids = unserialize($_COOKIE["carrello"], ["allowed_classes" => false]);
    if(!empty($ids)){
        foreach($ids as $id) {
            $newEvent = $dbh->getEventByID($id);
            $templateParams["eventi_carrello"] = array_merge($templateParams["eventi_carrello"], $newEvent); 
        }
    }
} else {
    setcookie("carrello", serialize(array()), time() + 86400 * 30, "/");
}
$templateParams["pageName"] = "Carrello";
$templateParams["nome"] = "template/lista_carrello.php";
if(!isset($_GET["onlyMain"]) || $_GET["onlyMain"] !== "true"){
    require("template/base.php");
} else {
    require("template/lista_carrello.php");
}
?>