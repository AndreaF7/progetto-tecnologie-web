<?php 
require_once("PHPMailer/PHPMailerAutoload.php");
require_once("db/databaseHelper.php");

/**This function return a new mail to send, only subject, body and adresses have to be set. */
function newMail(){
    $mail = new PHPMailer();
    $mail->isSMTP();
    $mail->SMTPAuth = true;
    $mail->SMTPAutoTLS = false;
    $mail->SMTPSecure = 'tls';
    $mail->Host = 'smtp.gmail.com';
    $mail->Port = 587;
    $mail->isHTML(true);
    $mail->Username = 'infoage.techweb@gmail.com'; //Inserire email di AGE.com
    $mail->Password = 'fichiSecchi20'; //Inserire email di AGE.com
    $mail->setFrom('infoage.techweb@gmail.com', 'AGE.com'); 
    return $mail;
}

function sendNotification($username, $email, $subject, $msg, $event){
    $dbh = new DataBaseHelper("localhost", "root", "", "age_db");
    //Record notification in db
    $id_notification = $dbh->insertNotification($subject, $msg, $event);
    $dbh->sendNotification($id_notification[0]["id_notifica"], $username);
    //Send notification via mail
    $mail = newMail();
    $mail->Subject = $subject;
    $mail->AddAddress($email);
    $mail->Body = $msg;
    $mail->send();
}
?>