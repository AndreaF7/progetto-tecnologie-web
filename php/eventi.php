<?php
require_once 'bootstrap.php';

$templateParams["pageName"] = "Tuoi eventi";
$templateParams["nome"] = "template/eventi_organizzatore.php";
if($_SESSION["tipo"] == "A"){
    $templateParams["eventi"] = $dbh->getEventsByUser("admin",5);
}else if($_SESSION["tipo"] == "O"){
    $templateParams["eventi"] = $dbh->getEventsByUser($_SESSION["username"],5);
} else{
    header("Location: index.php");
}

require 'template/base.php';
?>

